<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/')->name('page.home')->uses('HomeController@index');

Route::prefix('login')->middleware('guest')->group(function () {
    Route::get('/')->name('page.login')->uses('Auth\\LoginController@showLoginForm');
    Route::post('/')->uses('Auth\\LoginController@login');
});

Route::prefix('register')->middleware('guest')->group(function () {
    Route::get('/')->name('page.register')->uses('Auth\\RegisterController@showRegistrationForm');
    Route::post('/')->uses('Auth\\RegisterController@register');
});

Route::get('logout')->name('page.logout')->uses('Auth\\LoginController@logout');

Route::prefix('profile')->middleware('auth')->group(function () {
    Route::get('/')->name('page.profile.edit')->uses('ProfileController@edit');
    Route::post('/')->uses('ProfileController@update');
});

Route::prefix('forgot')->middleware('guest')->group(function () {
    Route::get('/')->name('page.forgot')->uses(
        'Auth\\ForgotPasswordController@showLinkRequestForm'
    );

    Route::post('/')->uses('Auth\\ForgotPasswordController@sendResetLinkEmail');
});

Route::prefix('password/reset/{token}')->middleware('guest')->group(function () {
    Route::get('/')->name('password.reset')->uses(
        'Auth\\ResetPasswordController@showResetForm'
    );

    Route::post('/')->uses('Auth\\ResetPasswordController@reset');
});

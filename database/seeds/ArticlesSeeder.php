<?php

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->chunkById(15, function (Collection $users) {
            foreach ($users as $user) {
                $count = mt_rand(1, 5);

                factory(Article::class, $count)->create([
                    'user_id' => $user,
                ]);
            }
        });
    }
}

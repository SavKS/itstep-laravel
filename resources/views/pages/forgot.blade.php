@extends('layouts.base')

@section('content')
    <h1 style="text-align: center" class="mt-5">Restore password</h1>

    @if(session()->has('status'))
        <h3>{{ session('status') }}</h3>
    @else
        {{ Form::open(['class' => 'mt-5']) }}

        <div class="form-group">
            {{ Form::label('email', 'Email address') }}
            {{ Form::email('email', null, ['class' => 'form-control  ' . ($errors->has('email') ? 'is-invalid' : '')]) }}

            @if($errors->has('email'))
                <div class="invalid-feedback">{{ implode('. ', $errors->get('email')) }}</div>
            @endif
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>

        {{ Form::close() }}
    @endif
@endsection

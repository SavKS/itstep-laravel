@extends('layouts.base')

@section('content')
    <h1 style="text-align: center" class="mt-5">Edit info</h1>

    {{ Form::model($user, ['class' => 'mt-5']) }}

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, ['class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : '')]) }}

        @if($errors->has('name'))
            <div class="invalid-feedback">{{ implode('. ', $errors->get('name')) }}</div>
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('email', 'Email address') }}
        {{ Form::email('email', null, ['class' => 'form-control  ' . ($errors->has('email') ? 'is-invalid' : '')]) }}

        @if($errors->has('email'))
            <div class="invalid-feedback">{{ implode('. ', $errors->get('email')) }}</div>
        @endif
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>

    {{ Form::close() }}
@endsection

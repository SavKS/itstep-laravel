@extends('layouts.base')

@section('title', 'Home page')

@section('content')
    @if(!Auth::check())
        <div class="d-flex align-items-center justify-content-center h1" style="height: 100vh">
            <a href="{{ route('page.login') }}">Login</a>
            <span class="ml-2 mr-2">/</span>
            <a href="{{ route('page.register') }}">Register</a>
        </div>
    @else
        Hello, {{ $user->name }}

        <div><a href="{{ route('page.profile.edit') }}">Edit profile</a></div>
        <div><a href="{{ route('page.logout') }}">Logout</a></div>
    @endif
@endsection

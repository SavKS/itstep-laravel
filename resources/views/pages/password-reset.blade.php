@extends('layouts.base')

@section('content')
    <h1 style="text-align: center" class="mt-5">Reset password</h1>

    {{ Form::open(['class' => 'mt-5']) }}

    <div class="form-group">
        {{ Form::label('email', 'Email address') }}
        {{ Form::email('email', $email, ['class' => 'form-control  ' . ($errors->has('email') ? 'is-invalid' : ''), 'readonly']) }}

        @if($errors->has('email'))
            <div class="invalid-feedback">{{ implode('. ', $errors->get('email')) }}</div>
        @endif
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password', ['class' => 'form-control  ' . ($errors->has('password') ? 'is-invalid' : '')]) }}

        @if($errors->has('password'))
            <div class="invalid-feedback">{{ implode('. ', $errors->get('password')) }}</div>
        @endif
    </div>
    <div class="form-group">
        {{ Form::label('password_confirmation', 'Password confirmation') }}
        {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
    </div>

    {{ Form::hidden('token', $token) }}

    <button type="submit" class="btn btn-primary">Submit</button>

    {{ Form::close() }}
@endsection

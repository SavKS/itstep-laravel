<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        dd(
            Auth::user()->articles->first()->user
        );

        return view('pages.home', [
            'user' => Auth::user(),
        ]);
    }
}

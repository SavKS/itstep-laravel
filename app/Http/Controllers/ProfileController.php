<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * @return View
     */
    public function edit(): View
    {
        return \view('pages.profile', [
            'user' => Auth::user(),
        ]);
    }

    public function update(UserUpdateRequest $request)
    {
        $user = Auth::user();

        $user->update(
            $request->validated()
        );

        return \redirect()->route('page.home');
    }
}

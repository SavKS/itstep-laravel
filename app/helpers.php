<?php

/**
 * @param string|\Illuminate\Database\Eloquent\Builder $query
 * @param array $params
 * @return string
 */
function interpolateQuery($query, array $params = []): string
{
    if (! is_string($query)) {
        $params = $query->getBindings();
        $query = $query->toSql();
    }

    return array_reduce($params, function (string $query, $param) {
        return substr_replace(
            $query,
            is_numeric($param) ? $param : "'{$param}'",
            strpos($query, '?'),
            1
        );
    }, $query);
}
